---
title: FAQ
sidebar_label: FAQ
---

###  How do I sponsor a feature?

Usually on github, projects will show the way to sponsor on the readme. If there's no obvious way, I usually just create an issue and offer to sponsor if they havent set up anything. 

###  What if I can't get a static IP?

If you can't upgrade  **AND** you have a namecheap API key, the following command will update all of you alias records. 

`NAMECHEAP_KEY=<YOUR namecheap key> sway namecheap update ip`

Also the [hoppy](https://hoppy.network) service has worked as a solution for some.

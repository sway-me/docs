import React, { useEffect, useState } from "react";

export default function Icons({ category, size = 55 }) {
  const [data, setData] = useState([]);

  useEffect(() => {
    let isMounted = true;
    fetch("https://minio.sway.cx/data/data.json")
      .then((res) => res.json())
      .then((json) => {
        if (isMounted) {
          setData(json.filter((j) => j.category === category));
        }
      });
    return () => {
      isMounted = false;
    };
  }, []);

  if (!data.length) {
    return <h5>...loading</h5>;
  }
  return (
    <div
      style={{
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "center",
      }}
    >
      {data.map(({ name }, i) => (
        <a
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            padding: "0.75rem",
            color: "slategray",
            fontSize: "64%",
            cursor: "pointer",
          }}
          key={i}
          href={`/intro/alternatives#${name}`}
        >
          <div style={{ height: size, width: size }} className={name} />
          <div
            style={{ wordWrap: "break-word", width: size, textAlign: "center" }}
          >
            {name}
          </div>
        </a>
      ))}
    </div>
  );
}

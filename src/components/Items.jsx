import React, { useEffect, useState } from "react";
import { round } from "../scripts/utils";

const LinkWithStars = ({ name, url, rating }) => (
  <span>
    {" "}
    &nbsp;| &nbsp; <a href={url}>{name}</a> <b>{round(rating)}</b> ⭐
  </span>
);

export default () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    let isMounted = true;
    fetch("https://minio.sway.cx/data/data.json")
      .then((res) => res.json())
      .then((json) => {
        if (isMounted) {
          setData(json);
        }
      });
    return () => {
      isMounted = false;
    };
  }, []);

  if (!data.length) {
    return <h1>...loading</h1>;
  }

  return data.map(({ name, description, ...rest }) => (
    <div key={name}>
      <hr />
      <div style={{ display: "flex", alignItems: "center" }}>
        <div className={name} />
        <div style={{ marginLeft: "1.5rem" }}>
          <h3>
            <a
              id={name}
              className="anchor enhancedAnchor_node_modules-@docusaurus-theme-classic-lib-next-theme-Heading-"
            />
            {name}
          </h3>
          <p
            style={{ marginBottom: 0 }}
            dangerouslySetInnerHTML={{ __html: description }}
          />
          <div>
            <b>Source Code:</b>
            {rest["repo-data"].map((props, i) => (
              <LinkWithStars key={i} name={name} {...props} />
            ))}
          </div>
        </div>
      </div>
    </div>
  ));
};
